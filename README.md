# Windows Virtualbox Runner

A simple script that let the user choose a VM and run it quickly.

## HOWTO
1. The script itself can be executed via powershell.
2. The script can be called from the command prompt
```
powershell -executionpolicy bypass -File vboxes.ps1
```
3. Using Win-PS2EXE the script can be compiled into an executable.